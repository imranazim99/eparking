### Read the following to setup you project

## Description
This Project is developed in VueJs frontend language and NodeJs backend language. Mongodb is used as database.

## Backend setup
cd to server and run the following command
```
npm install
```

## Compiles and hot-reloads for development
```
nodemon app.js
```

## Frontend setup
cd to client and run the following command
```
npm install
```
## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```

## Run your tests
```
npm run test
```

## Lints and fixes files
```
npm run lint
```

## Pull and push to repo
Make pull and push only use root folder. You must be in root folder i.e eparking

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
