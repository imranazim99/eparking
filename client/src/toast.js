import Vue from 'vue';
import Toasted from 'vue-toasted';
import Router from 'vue-router';
Vue.use(Toasted, {
    Router
});

export default () => {
    // options to the toast
    let optionsError = {
        type: 'error',
        icon: 'error_outline',
        position: 'top-right',
        duration: 5000
    };

    let optionsSuccess = {
        type: 'success',
        icon: 'check',
        position: 'top-right',
        duration: 5000
    };

    // register the toast with the custom message
    Vue.toasted.register('errorMsg',
        (payload) => {
            if (!payload.message) {
                return "Oops.. Something Went Wrong.."
            }
            return "Oops.. " + payload.message;
        }, optionsError);

    Vue.toasted.register('successMsg',
        (payload) => {
            if (!payload.message) {
                return "Record has been added successfully!"
            }
            return "Success! " + payload.message;
        }, optionsSuccess);
}
