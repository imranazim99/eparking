// cs
import './assets/css'
// js
import './assets/script'
// global
import './assets/global'
import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import Vuesax from "vuesax";
import "vuesax/dist/vuesax.css";
import Router from 'vue-router'
import store from './store'
import routes from "./routes"
import BootstrapVue from 'bootstrap-vue'
import "selectize/dist/css/selectize.bootstrap3.css";
import moment from "moment"
import "material-icons/iconfont/material-icons.css"
import * as VueGoogleMaps from 'vue2-google-maps'
import VueSimpleAlert from "vue-simple-alert"
import Toasted from './toast'

Vue.use(Vuex)
Vue.use(Vuesax)
Vue.use(Router)
Vue.use(BootstrapVue)
Vue.use(VueSimpleAlert)
Vue.use(Toasted)

Vue.use(VueGoogleMaps, {
    load: {
      key: 'AIzaSyAtZXg0silNQgaz3_vwi7ll7ErTMQe9q84',
      libraries: 'places',
      region: 'PK',
      // language: 'ur',
    }
})
const router = new Router({ mode: 'history', routes });
Vue.config.productionTip = false
// This will show date/time in formatted form
Vue.filter("formatDate", function (value) {
    if (value) {
        return moment(String(value)).format("DD-MM-YYYY");
    }
});
Vue.filter("formatDateString", function (value) {
    if (value) {
        return moment(String(value)).format("LL");
    }
});
Vue.filter("JSON", function (value) {
    if (value) {
        return JSON.stringify(value);
    }
});
Vue.filter("formatTime", function (value) {
    if (value) {
        return moment(String(value)).format("hh:mm:ss a");
    }
});
Vue.filter("formatDateTime", function (value) {
    if (value) {
        return moment(String(value)).format("DD-MM-YYYY hh:mm a");
    }
});
// Check each router before routing
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem("token") == null) {
            next({
                path: "/login",
                params: { nextUrl: to.fullPath }
            });
        } else {
            let user = JSON.parse(localStorage.getItem("currentUser"));
            if (to.meta.isPermission == "superAdmin") {
                if ( user && user.userType == "superAdmin") {
                    next()
                } else {
                    next({ path: '/login'})
                } 
            } else {
                next({ path: "/login", params: { nextUrl: to.fullPath } });
            }
        }
    } else {
        next();
    }
});
// End routing checking

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')