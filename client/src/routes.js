import Login from "./components/Login";
// Admin
import AdminDashboard from "./pages/Admin/Dashboard";
import Country from "./components/Country";
import Province from "./components/Province";
import City from "./components/City";
// Admin->MyCompany
import MyCompany from "./pages/Admin/Company/MyCompany";
import SoftwareUpdate from "./pages/Admin/Company/SoftwareUpdate";
import MyTeam from "./pages/Admin/Company/MyTeam";
import Branches from "./pages/Admin/Company/Branches";
// ./MyCompany
// Admin->MyCompanyUsers
import CreateUser from "./pages/Admin/companyUsers/CreateUser";
import UserDesignation from "./pages/Admin/companyUsers/UserDesignation";
import LoginHistory from "./pages/Admin/companyUsers/LoginHistory";
import UserBranch from "./pages/Admin/companyUsers/UserBranch";
// ./MyCompnayUsers
const routes = [
    // catch all redirect
    { path: '*', redirect: '/dashboard/home' },
    { path: "/", redirect: "/login" },
    { 
        path: "/login", 
        name: "login", 
        component: Login        
    },
    // Admin Dashbaord
    { 
        path: "/dashboard/home", 
        name: "AdminDashboard", 
        component: AdminDashboard,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    { 
        path: "/dashboard/country/show", 
        name: "Country", 
        component: Country,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    { 
        path: "/dashboard/province/show", 
        name: "Province", 
        component: Province,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    { 
        path: "/dashboard/city/show", 
        name: "City", 
        component: City,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    // my company
    { 
        path: "/dashboard/my-company/setting/show", 
        name: "MyCompany", 
        component: MyCompany,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    // software update
    { 
        path: "/dashboard/my-company/software-update/show", 
        name: "SoftwareUpdate", 
        component: SoftwareUpdate,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    // my team
    { 
        path: "/dashboard/my-company/myteam/show", 
        name: "MyTeam", 
        component: MyTeam,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    { 
        path: "/dashboard/my-company/branches/show", 
        name: "Branches", 
        component: Branches,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    // ./My Company

    // MyCompanyUsers
    { 
        path: "/dashboard/company-users/create-users/show", 
        name: "CreateUser", 
        component: CreateUser,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    // designation
    { 
        path: "/dashboard/company-users/user-designation/show", 
        name: "UserDesignation", 
        component: UserDesignation,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    { 
        path: "/dashboard/company-users/login-history/show", 
        name: "LoginHistory", 
        component: LoginHistory,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    { 
        path: "/dashboard/company-users/shift-to-branch/show", 
        name: "UserBranch", 
        component: UserBranch,
        meta: {
            requiresAuth: true,
            isPermission: "superAdmin"
        }
    },
    // ./MyCompanyUsers
]

export default routes;