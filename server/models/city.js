var mongoose=require("mongoose");
var citySchema =    new mongoose.Schema({
    title       :  {type: String, unique: true},
    province: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"Province"
    },
    isDelete: {type: Boolean, default: false},
    createdAt: Date,
    updatedAt: Date
});

module.exports  =   mongoose.model("City",citySchema);