var mongoose=require("mongoose");
var provinceSchema =    new mongoose.Schema({
    title       :  {type: String, unique: true},
    country: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"Country"
    },
    isDelete: {type: Boolean, default: false},
    createdAt: Date,
    updatedAt: Date
});

module.exports  =   mongoose.model("Province",provinceSchema);