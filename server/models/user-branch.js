var mongoose=require("mongoose");
var userBranchSchema =    new mongoose.Schema({
    company: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    createUser: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"CreateUser"
    },
    currentBranch: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"Branch",
        default: null
    },
    oldBranch: [{
        type:mongoose.Schema.Types.ObjectId,
        ref:"Branch",
        default: null
    }],
    isDelete: {type: Boolean, default: false},
    createdAt: Date,
    updatedAt: Date
});

module.exports  =   mongoose.model("UserBranch",userBranchSchema);