var mongoose = require("mongoose");
var teamSchema = new mongoose.Schema({
    name: String,
    fName: String,
    email: String,
    contactNo: String,

    // CNIC, passport or other personal number
    personalNo: {type: String, default: null},
    designation: String,
    memberSince: Date,
    image: String,

    // Link of a profile
    profileLink: {type: String, default: null},

    // active, inActive
    address: String,
    salary: {type: Number, default: null},
    status: {
        type: Boolean,
        default: false
    },
    isDelete: {type: Boolean, default: false},
    
    createdAt: Date,
    updatedAt: Date,
});
module.exports = mongoose.model("MyTeam", teamSchema);