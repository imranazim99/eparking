var mongoose = require("mongoose");
var softwareSchema = new mongoose.Schema({
    appName: {type: String},
    version: String,
    updatedOn: Date,
    operatingSystem: String,
    licenceNo: String,
    androidPath: String,
    iosPath: String,
    webPath: String,
    description: String,
    
    status: {
        type: Boolean,
        default: false
    },
    isDelete: {type: Boolean, default: false},
    
    createdAt: Date,
    updatedAt: Date,
});
module.exports = mongoose.model("SoftwareUpdate", softwareSchema);