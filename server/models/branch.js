var mongoose = require("mongoose");
var branchSchema = new mongoose.Schema({
    title: {type: String, required: true, unique: true},
    code : String,
    company: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"User"
    },
    city: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"City"
    },
    image: {
        type: String,
        default: null
    },
    location:{
        title:         String,
        latitude:      String,
        longitude:     String,
        icon:          String
    },
    address: {
        type: String,
        default: null
    },
    helpline: {
        type: String,
        default: null
    },
    contactNo: {
        type: String,
        default: null
    },
    contactPerson: {
        type: String,
        default: null
    },
    email: {
        type: String,
        default: null
    },
    website: {
        type: String,
        default: null
    },
    status: {
        type: Boolean,
        default: false
    },
    isActive: {type: Boolean, default: false},
    isDelete: {type: Boolean, default: false},

    createdAt: Date,
    updatedAt: Date,
});

module.exports = mongoose.model("Branch", branchSchema);