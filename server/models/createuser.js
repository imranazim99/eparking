var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");
var createUserSchema = new mongoose.Schema({
    username: {type: String, required: true},
    password: String,
    code : String,
    city: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"City"
    },
    designation: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"UserDesignation"
    },
    image: {
        type: String,
        default: null
    },
    name: String,
    fName: String,
    email: String,
    contactNo: String,

    // CNIC, passport or other personal number
    personalNo: {type: String, default: null},
    memberSince: Date,

    // active, inActive
    address: String,
    salary: {type: Number, default: null},
    status: {
        type: Boolean,
        default: false
    },
    last_login: {
        type: Date,
        default: null
    },
    token: { type: String, default: null},
    isDelete: {type: Boolean, default: false},
    
    createdAt: Date,
    updatedAt: Date,
});
createUserSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("CreateUser", createUserSchema);