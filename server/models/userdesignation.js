var mongoose=require("mongoose");
var designationSchema =    new mongoose.Schema({
    title:  {type: String, unique: true},
    isDelete: {type: Boolean, default: false},
    createdAt: Date,
    updatedAt: Date
});

module.exports  =   mongoose.model("UserDesignation",designationSchema);