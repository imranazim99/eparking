var mongoose=require("mongoose");
var countrySchema =    new mongoose.Schema({
    title:  {type: String, unique: true},
    code: String,
    isDelete: {type: Boolean, default: false},
    createdAt: Date,
    updatedAt: Date
});

module.exports  =   mongoose.model("Country",countrySchema);