var mongoose=require("mongoose");
var roleSchema =    new mongoose.Schema({
    title       :  {type: String, unique: true},
    permission  :  {
        type: String,
        enum: ['read','write','admin'],
        default: 'read'
    },
    createdAt: Date,
    updatedAt: Date
});

module.exports  =   mongoose.model("Role",roleSchema);