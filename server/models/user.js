var mongoose = require("mongoose");
var passportLocalMongoose = require("passport-local-mongoose");
var userSchema = new mongoose.Schema({
    username: {type: String, required: true},
    password: String,
    name: {type: String, required: true},
    code : String,
    city: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"City",
        default: null
    },
    role: {
        type:mongoose.Schema.Types.ObjectId,
        ref:"Role",
        default: null
    },
    image: {
        type: String,
        default: null
    },
    location:{
        title:         String,
        latitude:      String,
        longitude:     String,
        icon:          String
    },
    address: {
        type: String,
        default: null
    },
    helpline: {
        type: String,
        default: null
    },
    contactNo: {
        type: String,
        default: null
    },
    email: {
        type: String,
        default: null
    },
    website: {
        type: String,
        default: null
    },
    status: {
        type: Boolean,
        default: false
    },
    isActive: {type: Boolean, default: false},
    isDelete: {type: Boolean, default: false},
    userType: {
        type: String,
        default: "superAdmin"
    },
    createdAt: Date,
    updatedAt: Date,
});
userSchema.plugin(passportLocalMongoose);

module.exports = mongoose.model("User", userSchema);