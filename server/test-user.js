var host = "localhost";

var port = "9006";
var url = require("url"),
    fileUpload = require("express-fileupload"),
    express = require("express"),
    path = require("path"),
    passport = require('passport'),
    LocalStrategy = require("passport-local"),
    User = require("./models/user"),
    Role = require("./models/role"),
    bodyParser = require("body-parser"),
    mongoose = require('mongoose'),
    app = express();
    mongoose.connect("mongodb://localhost/eparking",{
        useCreateIndex: true,
        useNewUrlParser: true
    })
        .then(() => {
            console.log('Connected to the Database')
        })
        .catch(() => {
            console.log('Could not connected to the Database')
        });
app.set("view engine", "ejs");
app.use(express.static('public'));
app.use(express.static(__dirname + '/public'));
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(require("express-session")({
    secret:"I am the Best",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(fileUpload());
app.use(function(req,res,next){
    res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    res.locals.currentUser=req.user;
next();
});



// ========================= create role & user ========================
var data = {
    title : "TCKP",
    permission : "admin",
    createdAt : new Date
};
Role.create(data, (err, result) => {
    if (!err) {
        console.log(result);
        // Create new user
        let data = {
            "username": "imranazim99@gmail.com",
            "name": "Imran Azim",
            "role": result._id,
            "code": 123,
            "image":"/images/user.jpg",
            "createdAt": new Date

        };
        const password = '123';
        User.register(data, password, function (err, user) {
            if (err) {
            console.log(err);
            return;
            }
            console.log(user);
        })
    }
    else {
        console.log(err);
    }
});
