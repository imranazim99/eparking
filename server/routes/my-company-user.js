var express = require('express'),
Country = require("../models/country"),
Province = require("../models/province"),
City = require("../models/city"),
CreateUser = require("../models/createuser"),
UserDesignation = require("../models/userdesignation");

var fs = require("fs");
var router = express.Router();

// MY company create users
router.get("/dashboard/mycompany-users/users/get", (req, res) => {
    CreateUser.find({"isDelete": false}).populate("designation").populate({path: "city", populate: {path: "province", populate: {path: "country"}}}).exec((err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// add record
router.post("/dashboard/mycompany-users/user/add", (req, res) => {
    if(req.files && req.files.image != undefined){
        var dir = "./public/uploads/company/";
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        let sampleFile = req.files.image;
        if (sampleFile != undefined && sampleFile.name != undefined) {
            var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
        }
    
        //Use the mv() method to place the file somewhere on your server
        sampleFile.mv(imagename, function (err) {
            if (err) {
                res.send({error: err.message});
            }
        });
        imagename = imagename.substring(8);
        var inputs = new CreateUser({
            username: req.body.email,
            name: req.body.name,
            fName: req.body.fName,
            memberSince: new Date(req.body.memberSince),
            contactNo: req.body.contactNo,
            email: req.body.email,
            personalNo: req.body.personalNo,
            designation: req.body.designation,
            code: req.body.code,
            address: req.body.address,
            city: req.body.city,
            image: imagename,
            createdAt: new Date,
        });
        CreateUser.register(inputs, req.body.code, function (err, newUser) {
            if (err) {
                res.send({error: err.message});
            } else {
                // Data saved successfully!
                res.send({success: "Success"});
            }
        })
    } else {
        res.send({error: "Upload profile image please!"});
    }
})
// edit record
router.get("/dashboard/mycompany-users/user/:id/edit", (req, res) => {
    CreateUser.findOne({"_id": req.params.id}, (err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// update record
router.post("/dashboard/mycompany-users/user/update", (req, res) => {
    CreateUser.findOne({"_id": req.body.userId}, (err, team) => {
        if(req.files && req.files.image != undefined){
            var dir = "./public/uploads/company/";
            if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            }
            let sampleFile = req.files.image;
            if (sampleFile != undefined && sampleFile.name != undefined) {
                var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
            }
        
            //Use the mv() method to place the file somewhere on your server
            sampleFile.mv(imagename, function (err) {
                if (err) {
                    res.send({error: err.message});
                }
            });
            imagename = imagename.substring(8);
            team.username = req.body.email;
            team.name = req.body.name;
            team.fName = req.body.fName;
            team.memberSince = new Date(req.body.memberSince);
            team.contactNo = req.body.contactNo;
            team.email = req.body.email;
            team.personalNo = req.body.personalNo;
            team.designation = req.body.designation;
            team.address = req.body.address;
            team.city = req.body.city;
            team.image = imagename;
            team.updatedAt = new Date;
            team.save((err, newRecord) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        } else {
            team.username = req.body.email;
            team.name = req.body.name;
            team.fName = req.body.fName;
            team.memberSince = new Date(req.body.memberSince);
            team.contactNo = req.body.contactNo;
            team.email = req.body.email;
            team.personalNo = req.body.personalNo;
            team.designation = req.body.designation;
            team.address = req.body.address;
            team.city = req.body.city;
            team.updatedAt = new Date;
            team.save((err, newRecord) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        }
    });
});

// ./MY Compnay create users

// User designations
// show designation
router.get('/dashboard/mycompany-users/user-designation/get', function(req, res) {
    UserDesignation.find({isDelete: false}).sort("-createdAt").exec((err, result) => {
        res.send(result);
    })
});
// add designation
router.post('/dashboard/mycompany-users/user-designation/add', function (req, res) {
    UserDesignation.findOne({title: req.body.title}, (err, found) => {
        if(found){
            found.isDelete = false;
            found.save();
            res.send({success: found.title+" was already exist and deleted. Now it is restored."});
        } else {
            if (req.body.title != "") {
                var record = new UserDesignation({
                    title: req.body.title,
                    createdAt: new Date
                });
                record.save((err, result) => {
                    if (err) {
                        res.send({ error: err.message });
                    } else {
                        res.send({ success: "Record has been added successfully." });
                    }
                })
            } else {
                res.send({error: "Title is required."});
            }
        }
    })
});
// edit designaiton
router.get('/dashboard/mycompany-users/user-designation/:id/edit', function(req, res) {
    UserDesignation.findOne({_id: req.params.id}, (err, record) => {
        res.send(record);
    })
});
// udpate designation
router.post('/dashboard/mycompany-users/user-designation/update', function (req, res) {
    if (req.body.title && req.body.desigId) {
        var inputs = {
            title: req.body.title,
            updatedAt: new Date
        };
    UserDesignation.findOneAndUpdate({ _id: req.body.desigId },
        inputs
        , (err, record) => {
            if (err) {
                res.send({ error: err.message });
            } else {
                res.send({ success: record });
            }
        })
    } else {
        res.send({ error: "Something went wrong!" });
    }
});
// delete country
router.get('/dashboard/mycompany-users/user-designation/:id/delete', (req, res) => {
    UserDesignation.findOne({ _id: req.params.id },(err, record) => {
        if (err) {
            res.send({ error: err.message });
        } else {
            record.isDelete = true;
            record.save();
            res.send({ success: "deleted" });
        }
    })
})
// ./User designations

// login history of my company users
router.get("/dashboard/mycompany-users/login-history/get", (req, res) => {
    CreateUser.find({"isDelete": false}).populate("designation")
    .select({"name": true, "username": true, "designation": true, "memberSince": true, "last_login": true, "status": true})
    .exec((err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
//  ./ login history
module.exports = router;