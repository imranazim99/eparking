var express = require('express'),
Country = require("../models/country"),
Province = require("../models/province"),
City = require("../models/city");
var router = express.Router();

// Country
// show country
router.get('/dashboard/country/get', function(req, res) {
    Country.find({isDelete: false}, (err, countries) => {
        res.send(countries);
    })
});
// add country
router.post('/dashboard/country/add', function (req, res) {
    if (req.body.countryId == "") {
        if (req.body.title && req.body.code) {
            var country = new Country({
                title: req.body.title,
                code: req.body.code,
                createdAt: new Date
            });
            country.save((err, result) => {
                if (err) {
                    res.send({ error: err.message });
                } else {
                    res.send({ success: result });
                }
            })
        }
    } else {
        if (req.body.title && req.body.code && req.body.countryId) {
            var inputs = {
                title: req.body.title,
                code: req.body.code,
                updatedAt: new Date
            };
            Country.findOneAndUpdate({ _id: req.body.countryId },
                inputs
                , (err, record) => {
                    if (err) {
                        res.send({ error: err.message });
                    } else {
                        res.send({ success: record });
                    }
                })
        } else {
            res.send({ error: "Something went wrong!" });
        }
    }
});
// edit country
router.get('/dashboard/country/:id/edit', function(req, res) {
    Country.findOne({_id: req.params.id}, (err, record) => {
        res.send(record);
    })
});
// delete country
router.get('/dashboard/country/:id/delete', (req, res) => {
    Country.findOneAndUpdate({ _id: req.params.id },{isDelete: true},(err, record) => {
        if (err) {
            res.send({ error: err.message });
        } else {
            res.send({ success: "deleted" });
        }
    })
})
// ./Country

// Province
// show all
router.get('/dashboard/province/get', function(req, res) {
    Province.find({isDelete: false}).populate("country").exec((err, provinces) => {
        res.send(provinces);
    })
});
// add & update
router.post('/dashboard/province/add', function (req, res) {
    if (req.body.provinceId == "") {
        if (req.body.title && req.body.countryId) {
            var province = new Province({
                title: req.body.title,
                country: req.body.countryId,
                createdAt: new Date
            });
            province.save((err, result) => {
                if (err) {
                    res.send({ error: err.message });
                } else {
                    res.send({ success: result });
                }
            })
        }
    } else {
        if (req.body.title && req.body.provinceId && req.body.countryId) {
            var inputs = {
                title: req.body.title,
                country: req.body.countryId,
                updatedAt: new Date
            };
            Province.findOneAndUpdate({ _id: req.body.provinceId },
                inputs
                , (err, record) => {
                    if (err) {
                        res.send({ error: err.message });
                    } else {
                        res.send({ success: record });
                    }
                })
        } else {
            res.send({ error: "Something went wrong!" });
        }
    }
});
// edit
router.get('/dashboard/province/:id/edit', function(req, res) {
    Province.findOne({_id: req.params.id}, (err, record) => {
        res.send(record);
    })
});
// delete
router.get('/dashboard/province/:id/delete', (req, res) => {
    Province.findOneAndUpdate({ _id: req.params.id },{isDelete: true},(err, record) => {
        if (err) {
            res.send({ error: err.message });
        } else {
            res.send({ success: "deleted" });
        }
    })
})
// ./Province

// City/District
// show all
router.get('/dashboard/city/get', function(req, res) {
    City.find({isDelete: false}).populate({path: "province", populate: {path: "country"}}).exec((err, cities) => {
        res.send(cities);
    })
});
// 
router.get('/dashboard/city/province/:id/get', function(req, res) {
    Province.find({country: req.params.id, isDelete: false}, (err, record) => {
        res.send(record);
    })
});
// add & update
router.post('/dashboard/city/add', function (req, res) {
    if (req.body.distId == "") {
        if (req.body.title && req.body.countryId && req.body.provinceId) {
            var city = new City({
                title: req.body.title,
                province: req.body.provinceId,
                createdAt: new Date
            });
            city.save((err, result) => {
                if (err) {
                    res.send({ error: err.message });
                } else {
                    res.send({ success: result });
                }
            })
        }
    } else {
        if (req.body.title && req.body.provinceId && req.body.countryId && req.body.distId) {
            var inputs = {
                title: req.body.title,
                province: req.body.provinceId,
                updatedAt: new Date
            };
            City.findOneAndUpdate({ _id: req.body.distId },
                inputs
                , (err, record) => {
                    if (err) {
                        res.send({ error: err.message });
                    } else {
                        res.send({ success: record });
                    }
                })
        } else {
            res.send({ error: "Something went wrong!" });
        }
    }
});
// edit
router.get('/dashboard/city/:id/edit', function(req, res) {
    City.findOne({_id: req.params.id}).populate("province").exec((err, record) => {
        res.send(record);
    })
});
// delete
router.get('/dashboard/city/:id/delete', (req, res) => {
    City.findOneAndUpdate({ _id: req.params.id },{isDelete: true},(err, record) => {
        if (err) {
            res.send({ error: err.message });
        } else {
            res.send({ success: "deleted" });
        }
    })
})
// ./City/District
module.exports = router;
