var express = require('express');
var router = express.Router();

// ====== Routers Modules =========
// including routers modules
var login = require('./login'),
    company = require('./company'),
    country = require('./country'),
    userBranch = require('./user-branch'),
    myCompanyUsers = require('./my-company-user');

// Using routers modules
router.use(login);
router.use(country);
router.use(company);
router.use(myCompanyUsers);
router.use(userBranch);
// ====== End of Routers Modules =======
/* GET home page. */
router.get('/', function(req, res, next) {
  res.send('Welcome! Nodejs server is running and live.');
});

module.exports = router;
