var express = require('express'),
    passport = require("passport"),
    jwt = require("jsonwebtoken"),
    jwtConfig = require("../config/jwtConfig"),
    fs = require("fs"),
    User = require("../models/user"),
    Role = require("../models/role"),
    router = express.Router();

// This will login user as a admin into system
router.post('/login/user', (req, res, next) => {
    passport.authenticate('local', function(err, user, info) {
        if (err) {
            return res.send({ error: '' + err.message }); // will generate a 500 error
        }
        if (!user) {
            return res.send({ error: '' + info.message });
        } else {
            const token = jwt.sign({ username: user.username }, jwtConfig.secret, { expiresIn: 60 * 60 * 24 });
            // console.log(token)
            return res.send({
                success: {
                    message: 'Login is successful.',
                    user: user,
                    userType: user.userType,
                    auth: true,
                    token: token
                }
            });
        }
    })(req, res, next);
})
// Add role
router.get("/admin/role-for-admin/add", (req, res) => {
    var role = new Role;
    role.title = "Super Admin";
    role.permission = "admin";
    role.createdAt = new Date;
    role.save((err, newRole) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send({success: "Created Successfully!", data: newRole});
        }
    })
})
// This method is used to register new user as a admin
router.get("/admin/user/admin/register", function (req, res) {
    Role.findOne({"permission": "admin", "title": "Super Admin"}, (err, role) => {
    var dir = "./public/uploads/user/";
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
    var imagename = "/uploads/user/user.jpg";

    var password = "admin";
    var newUser = new User({
        username: "imranazim99@gmail.com",
        email: "imranazim99@gmail.com",
        name: "Imran Azim",
        code: password,
        role: role._id,
        image: imagename,
        createdAt: new Date,
    });
    User.register(newUser, password, function (err, user) {
        if (err) {
            res.send({error: err.message});
        } else {
            res.send({success: 'Congratulations! '+user.name+' your account has been created successfully. Now you can Sign in.'});
        }
    });
    });
});
// this will logout user from server side
router.get('/logout', (req, res) => {
    req.logout();
    console.log('Logout done');
    res.status(200).send();
})


module.exports = router;