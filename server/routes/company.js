var express = require('express'),
Country = require("../models/country"),
User = require("../models/user"),
Province = require("../models/province"),
fs = require("fs"),
SoftwareUpdate = require("../models/softwareupdate"),
MyTeam = require("../models/myteam"),
City = require("../models/city"),
Branch = require("../models/branch"),
shortid = require('shortid');
var router = express.Router();

// my company
// show companies
router.get('/dashboard/my-company/get', function(req, res) {
    User.find({}).populate({path: "city"})
    .exec((err, records) => {
        res.send(records);
    })
});
// this adds new record
router.post('/dashboard/my-company/add', function(req, res) {
    if(req.files && req.files.image != undefined){
    var placeLocation = JSON.parse(req.body.location);
    var dir = "./public/uploads/company/";
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    let sampleFile = req.files.image;
    if (sampleFile != undefined && sampleFile.name != undefined) {
        var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
    }

    //Use the mv() method to place the file somewhere on your server
    sampleFile.mv(imagename, function (err) {
        if (err) {
            res.send({error: err.message});
        }
    });
    imagename = imagename.substring(8);
    var newCompany = new User({
        username: req.body.email,
        code: req.body.code,
        name: req.body.name,
        location: {
            title: placeLocation.title,
            latitude: placeLocation.latitude,
            longitude: placeLocation.longitude,
            icon: placeLocation.icon
        },
        contactNo: req.body.contactNo,
        helpline: req.body.helpline,
        website: req.body.website,
        email: req.body.email,
        address: req.body.address,
        image: imagename,
        city: req.body.city,
        createdAt: new Date
    });
    User.register(newCompany, req.body.code, function (err, company) {
        if (err) {
            res.send({error: err.message});
        } else {
            // Data saved successfully!
            res.send({success: "Success"});
        }
    })
    }
});
// this edit record
router.get('/dashboard/my-company/:id/edit', function(req, res) {
    User.findOne({"_id": req.params.id})
    .exec((err, records) => {
        res.send(records);
    })
});
// update record
router.post('/dashboard/my-company/update', function(req, res) {
    User.findOne({_id: req.body.companyId}, (err, company) => {
        if(req.files && req.files.image != undefined){
            var placeLocation = JSON.parse(req.body.location);
            var dir = "./public/uploads/company/";
            if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
            }
            let sampleFile = req.files.image;
            if (sampleFile != undefined && sampleFile.name != undefined) {
                var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
            }
        
            //Use the mv() method to place the file somewhere on your server
            sampleFile.mv(imagename, function (err) {
                if (err) {
                    res.send({error: err.message});
                }
            });
            imagename = imagename.substring(8);
            company.name = req.body.name;
            company.location = {
                title: placeLocation.title,
                latitude: placeLocation.latitude,
                longitude: placeLocation.longitude,
                icon: placeLocation.icon
            },
            company.contactNo = req.body.contactNo;
            company.helpline = req.body.helpline;
            company.website = req.body.website;
            company.email = req.body.email;
            company.address = req.body.address;
            company.image = imagename;
            company.city = req.body.city;
            company.updatedAt = new Date;
            company.save((err, companyUpdated) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        } else {
            var placeLocation = JSON.parse(req.body.location);
            company.name = req.body.name;
            company.location = {
                title: placeLocation.title,
                latitude: placeLocation.latitude,
                longitude: placeLocation.longitude,
                icon: placeLocation.icon
            },
            company.contactNo = req.body.contactNo;
            company.helpline = req.body.helpline;
            company.website = req.body.website;
            company.email = req.body.email;
            company.address = req.body.address;
            company.city = req.body.city;
            company.updatedAt = new Date;
            company.save((err, companyUpdated) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        }
    }) 
});

// SOFTWARE UPDATES
router.get("/dashboard/software-update/get", (req, res) => {
    SoftwareUpdate.find({"isDelete": false}, (err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// add record
router.post("/dashboard/software-update/add", (req, res) => {
    var softwareUpdate = new SoftwareUpdate;
    softwareUpdate.appName = req.body.appName;
    softwareUpdate.version = req.body.version;
    softwareUpdate.updatedOn = new Date(req.body.updatedOn);
    softwareUpdate.licenceNo = req.body.licenceNo;
    softwareUpdate.androidPath = req.body.androidPath;
    softwareUpdate.iosPath = req.body.iosPath;
    softwareUpdate.webPath = req.body.webPath;
    softwareUpdate.description = req.body.description;
    softwareUpdate.createdAt = new Date;
    softwareUpdate.save((err, newRecord) => {
        if (err) {
            res.send({error: err.message});
        } else {
            res.send({success: "Success"});
        }
    })
})
// edit record
router.get("/dashboard/software-update/:id/edit", (req, res) => {
    SoftwareUpdate.findOne({"_id": req.params.id}, (err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// update record
router.post("/dashboard/software-update/update", (req, res) => {
    if(req.body.softwareId && req.body.softwareId != undefined){
        SoftwareUpdate.findOne({"_id": req.body.softwareId}, (err, software) => {
            software.appName = req.body.appName;
            software.version = req.body.version;
            software.updatedOn = new Date(req.body.updatedOn);
            software.licenceNo = req.body.licenceNo;
            software.androidPath = req.body.androidPath;
            software.iosPath = req.body.iosPath;
            software.webPath = req.body.webPath;
            software.description = req.body.description;
            software.updatedAt = new Date;
            software.save((err, newRecord) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        })
    }
})
// ./SOFTWARE UPDATES
// MY TEAM
router.get("/dashboard/myteam/get", (req, res) => {
    MyTeam.find({"isDelete": false}, (err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// add record
router.post("/dashboard/myteam/add", (req, res) => {
    var team = new MyTeam;
    if(req.files && req.files.image != undefined){
        var dir = "./public/uploads/company/";
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
        }
        let sampleFile = req.files.image;
        if (sampleFile != undefined && sampleFile.name != undefined) {
            var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
        }
    
        //Use the mv() method to place the file somewhere on your server
        sampleFile.mv(imagename, function (err) {
            if (err) {
                res.send({error: err.message});
            }
        });
        imagename = imagename.substring(8);
        team.name = req.body.name;
        team.fName = req.body.fName;
        team.memberSince = new Date(req.body.memberSince);
        team.contactNo = req.body.contactNo;
        team.email = req.body.email;
        team.personalNo = req.body.personalNo;
        team.designation = req.body.designation;
        team.salary = req.body.salary;
        team.address = req.body.address;
        team.profileLink = req.body.profileLink;
        team.image = imagename;
        team.createdAt = new Date;
        team.save((err, newRecord) => {
            if (err) {
                res.send({error: err.message});
            } else {
                res.send({success: "Success"});
            }
        })
    } else {
        res.send({error: "Upload profile image please!"});
    }
})
// edit record
router.get("/dashboard/myteam/:id/edit", (req, res) => {
    MyTeam.findOne({"_id": req.params.id}, (err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// update record
router.post("/dashboard/myteam/update", (req, res) => {
    MyTeam.findOne({"_id": req.body.teamId}, (err, team) => {
        if(req.files && req.files.image != undefined){
            var dir = "./public/uploads/company/";
            if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
            }
            let sampleFile = req.files.image;
            if (sampleFile != undefined && sampleFile.name != undefined) {
                var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
            }
        
            //Use the mv() method to place the file somewhere on your server
            sampleFile.mv(imagename, function (err) {
                if (err) {
                    res.send({error: err.message});
                }
            });
            imagename = imagename.substring(8);
            team.name = req.body.name;
            team.fName = req.body.fName;
            team.memberSince = new Date(req.body.memberSince);
            team.contactNo = req.body.contactNo;
            team.email = req.body.email;
            team.personalNo = req.body.personalNo;
            team.designation = req.body.designation;
            team.salary = req.body.salary;
            team.address = req.body.address;
            team.profileLink = req.body.profileLink;
            team.image = imagename;
            team.updatedAt = new Date;
            team.save((err, newRecord) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        } else {
            team.name = req.body.name;
            team.fName = req.body.fName;
            team.memberSince = new Date(req.body.memberSince);
            team.contactNo = req.body.contactNo;
            team.email = req.body.email;
            team.personalNo = req.body.personalNo;
            team.designation = req.body.designation;
            team.salary = req.body.salary;
            team.address = req.body.address;
            team.profileLink = req.body.profileLink;
            team.updatedAt = new Date;
            team.save((err, newRecord) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        }
    });
});

// ./MY TEAM

// Branches.
// get all branches
router.get('/dashboard/my-company/branches/get', function(req, res) {
    Branch.find({isDelete: false}).populate("company city")
    .exec((err, records) => {
        res.send(records);
    })
});
// add branch
router.post('/dashboard/my-company/branch/add', function(req, res) {
    if(req.files && req.files.image != undefined){
    // {"route":"Peshawar Ring Road","locality":"Peshawar","administrative_area_level_1":"Khyber Pakhtunkhwa","country":"Pakistan","latitude":34.0343067,"longitude":71.5413064}
    var placeLocation = JSON.parse(req.body.location);
    var dir = "./public/uploads/company/branches/";
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir);
    }
    let sampleFile = req.files.image;
    if (sampleFile != undefined && sampleFile.name != undefined) {
        var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
    }

    //Use the mv() method to place the file somewhere on your server
    sampleFile.mv(imagename, function (err) {
        if (err) {
            res.send({error: err.message});
        }
    });
    imagename = imagename.substring(8);
    var branch = {
        company: req.body.company,
        title: req.body.title,
        code: shortid.generate(),
        location: {
            title: placeLocation.title,
            latitude: placeLocation.latitude,
            longitude: placeLocation.longitude,
            icon: placeLocation.icon
        },
        contactNo: req.body.contactNo,
        contactPerson: req.body.contactPerson,
        helpline: req.body.helpline,
        website: req.body.website,
        email: req.body.email,
        address: req.body.address,
        image: imagename,
        city: req.body.city,
        createdAt: new Date
    };
    Branch.create(branch, function (err, newBranch) {
        if (err) {
            res.send({error: err.message});
        } else {
            // Data saved successfully!
            res.send({success: "Success"});
        }
    })
    }
});
// this edit record
router.get('/dashboard/my-company/branch/:id/edit', function(req, res) {
    Branch.findOne({"_id": req.params.id})
    .exec((err, records) => {
        res.send(records);
    })
});

router.post('/dashboard/my-company/branch/update', function(req, res) {
    Branch.findOne({_id: req.body.branchId}, (err, record) => {
        if(req.files && req.files.image != undefined){
            // {"route":"Peshawar Ring Road","locality":"Peshawar","administrative_area_level_1":"Khyber Pakhtunkhwa","country":"Pakistan","latitude":34.0343067,"longitude":71.5413064}
            var placeLocation = JSON.parse(req.body.location);
            var dir = "./public/uploads/company/branches/";
            if (!fs.existsSync(dir)) {
              fs.mkdirSync(dir);
            }
            let sampleFile = req.files.image;
            if (sampleFile != undefined && sampleFile.name != undefined) {
                var imagename = dir + Date.now()/1000 + "-" + sampleFile.name;
            }
        
            //Use the mv() method to place the file somewhere on your server
            sampleFile.mv(imagename, function (err) {
                if (err) {
                    res.send({error: err.message});
                }
            });
            imagename = imagename.substring(8);
            record.company = req.body.company;
            record.title = req.body.title;
            record.location = {
                title: placeLocation.title,
                latitude: placeLocation.latitude,
                longitude: placeLocation.longitude,
                icon: placeLocation.icon
            },
            record.contactNo = req.body.contactNo;
            record.contactPerson = req.body.contactPerson;
            record.helpline = req.body.helpline;
            record.website = req.body.website;
            record.email = req.body.email;
            record.address = req.body.address;
            record.image = imagename;
            record.city = req.body.city;
            record.updatedAt = new Date;
            record.save((err, branchUpdated) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        } else {
            var placeLocation = JSON.parse(req.body.location);
            record.title = req.body.title;
            record.company = req.body.company;
            record.location = {
                title: placeLocation.title,
                latitude: placeLocation.latitude,
                longitude: placeLocation.longitude,
                icon: placeLocation.icon
            },
            record.contactNo = req.body.contactNo;
            record.contactPerson = req.body.contactPerson;
            record.helpline = req.body.helpline;
            record.website = req.body.website;
            record.email = req.body.email;
            record.address = req.body.address;
            record.city = req.body.city;
            record.updatedAt = new Date;
            record.save((err, branchUpdated) => {
                if (err) {
                    res.send({error: err.message});
                } else {
                    res.send({success: "Success"});
                }
            })
        }
    }) 
});
// delete record
router.get('/dashboard/my-company/branch/:id/delete', (req, res) => {
    Branch.findOneAndUpdate({ _id: req.params.id },{isDelete: true},(err, record) => {
        if (err) {
            res.send({ error: err.message });
        } else {
            res.send({ success: "deleted" });
        }
    })
})
//  ./ Branches
module.exports = router;