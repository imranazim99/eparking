var express = require('express'),
UserBranch = require("../models/user-branch"),
CreateUser = require("../models/createuser"),
Branch = require("../models/branch"),
Company = require("../models/user");

var fs = require("fs");
var router = express.Router();

// MY company create users
router.get("/dashboard/mycompany-users/shift-to-branch/get", (req, res) => {
    UserBranch.find({"isDelete": false})
    .populate({
        path: "company",
        select: "name"
    })
    .populate({
        path: "createUser",
        select: "name"
    })
    .populate({
        path: "currentBranch",
        select: "title"
    })
    .populate([{
        path: "oldBranch",
        select: "title"
    }])
    .exec((err, records) => {
        if(err){
            return res.send({error: err.message});
        }
        return res.send(records);
    });
});
router.get("/dashboard/mycompany-users/branches/dropdown", (req, res) => {
    Branch.find({"isDelete": false}).select({"title": true, "code": true}).exec((err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
router.get("/dashboard/mycompany-users/users/dropdown", (req, res) => {
    CreateUser.find({"isDelete": false}).select({"name": true, "email": true}).exec((err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// add record
router.post("/dashboard/mycompany-users/shift-to-branch/user/add", (req, res) => {
    if (req.body && req.body.user && req.body.company && req.body.branch) {
        UserBranch.findOne({ "createUser": req.body.user, "currentBranch": req.body.branch }, (err, branch) => {
            if (branch) {
                res.send({ error: "This user has already in this branch." });
            } else {
                let inputs = {
                    company: req.body.company,
                    currentBranch: req.body.branch,
                    createUser: req.body.user,
                    createdAt: new Date
                }
                UserBranch.create(inputs, function (err, newRecord) {
                    if (err) {
                        res.send({ error: err.message });
                    } else {
                        // Data saved successfully!
                        res.send({ success: "Success" });
                    }
                })
            }
        })
    } else {
        res.send({error: "Something went wrong!"});
    }
})
// edit record
router.get("/dashboard/mycompany-users/shift-to-branch/:id/edit", (req, res) => {
    UserBranch.findOne({"_id": req.params.id}, (err, items) => {
        if(err){
            res.send({error: err.message});
        } else {
            res.send(items);
        }
    });
});
// update record
router.post("/dashboard/mycompany-users/shift-to-branch/user/update", async (req, res) => {
    if (req.body && req.body.userBranchId != undefined && req.body.user != undefined && req.body.company !=undefined && req.body.branch != undefined) {
        let checkBranch = await UserBranch.findOne({ "createUser": req.body.user, "currentBranch": req.body.branch });
            if (checkBranch) {
                res.send({ success: "Nothing updated, user is on the current branch." });
            } else {
                let userBranch = await UserBranch.findOne({"createUser": req.body.user});
                if(userBranch) {
                    userBranch.oldBranch.push(userBranch.currentBranch);
                    userBranch.currentBranch = req.body.branch;
                    userBranch.updatedAt = new Date
                    userBranch.save((err, updatedBranch) => {
                        if(!err) {
                            UserBranch.findOne({"_id": updatedBranch._id}).populate({path: "currentBranch", select: "title"})
                            .exec((err, updatedBranch) => {
                                if(err) {
                                    return res.send({ error: err.message });
                                }
                                return res.send({ success: `User is shifted to ${updatedBranch.currentBranch.title} successfully.`});
                            })
                        } else {
                            res.send({ error: err.message });
                        }
                    });
                }
            }
    } else {
        res.send({error: "Something went wrong!"});
    }
});

// delete record temporary
router.get("/dashboard/mycompany-users/shift-to-branch/:id/delete", (req, res) => {
    if(req.params.id != undefined) {
        UserBranch.findOneAndRemove({"_id": req.params.id}, (err, record) => {
            if(err){
                return res.send({error: err.message});
            }
            return res.send({ success: "User and his branch has been removed successfully."});
        });
    }
});

module.exports = router;