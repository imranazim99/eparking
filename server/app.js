var createError = require('http-errors'),
    express = require('express'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    fileUpload = require('express-fileupload'),
    bodyParser = require('body-parser'),
    path = require('path'),
    session = require('express-session'),
    router = require('./routes/index'),
    flash = require('express-flash'),
    passport = require("passport"),
    LocalStrategy = require("passport-local").Strategy,
    mongoose = require('mongoose'),
    env=require('dotenv'),
    User = require("./models/user"),
    Role = require("./models/role"),

    fs = require('fs');
var router = require('./routes/index');
var app = express();
var http = require('http').Server(app);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// Connect to mongodb
mongoose.connect("mongodb://localhost/eparking",{
    useUnifiedTopology: true,    
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
}).then(() => {
        console.log('Connected to Mongodb');
    })
    .catch(() => {
        console.log('Could not connected to the Database');
    });

// Express session
app.use(session({
  secret: "insideNow",
  resave: false,
  saveUninitialized: false,
  cookie  : { maxAge  : 30 * 24 * 60 * 60 * 1000 }
}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));

//serialize
passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  User.findOne({
      "username": user.username
  }).exec(function (err, data) {
      if (err) {
          console.log("Error:", err);
      } else {
          done(null, data);
      }
  });
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); // configure template engine
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// Tell the bodyparser middleware to accept more data
app.use(bodyParser.json({ limit: '50mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

app.use(express.static(path.join(__dirname, 'public')));
// configure express to use public folder
app.use(express.static(__dirname + '/static'));

app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(fileUpload()); // configure fileupload

app.use(router);

app.use(function (req, res, next) {
  res.set('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
  res.locals.currentUser = req.user;
  next();
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
// });

// set the app to listen on the port
const port = (process.env.PORT || "9005");
var server = http.listen(port, () => {
    console.log('NodeJs server is running on port: ', server.address().port);
  });
